<?php
require_once __DIR__ . '/vendor/autoload.php';

use harpya\communicator\Client;

if (!getenv(Client::DISCOVER_URL)) {
    // Ignoring, since there is no Discover instance configured
    echo "\n To perform the functional tests, is necessary have a valid Discovery instance configured\n";
    exit;
}

$serviceSpec = [
    'name' => env('MICRO_SERVICE_SAMPLE_NAME', 'service1'),
    'version' => env('MICRO_SERVICE_SAMPLE_VERSION', 1),
    'host' => env('MICRO_SERVICE_SAMPLE_HOST', 'http://service1'),
    'port' => env('MICRO_SERVICE_SAMPLE_PORT', 8765)
];

echo "\n Starting service with specs: " . print_r($serviceSpec,true)."\n";

$client = new Client();
$client->init();
$response = $client->createService($serviceSpec);

print_r($response);
echo "\n\n\n";
