FROM php:7.2-cli

WORKDIR /srv/app

COPY ./src /srv/app

RUN     apt-get update \
        && apt-get -y install --no-install-recommends curl git unzip vim \
        && apt-get autoremove \
        && apt-get autoclean \
        && rm -rf /var/lib/apt/lists/*

RUN curl --silent --show-error https://getcomposer.org/installer | php \
    && mv composer.phar /usr/bin/composer

ENTRYPOINT ["/bin/bash","-c","/srv/app/startup.sh"]


